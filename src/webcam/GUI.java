package webcam;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class GUI extends JPanel {
	private BufferedImage image;
	JFrame frame = new JFrame();
	
	public GUI(String windowName) {
		frame.setTitle(windowName);
		createWindow();
	}
	
	private void createWindow() {
		frame.setContentPane(this);
		frame.setSize(1320, 780);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	private BufferedImage getImage() {
		return image;
	}
	
	public void setImage(BufferedImage image) {
		this.image = image;
	}
	
	public void paintComponent(Graphics g) {
		BufferedImage temp = getImage();
		g.drawImage(temp, 10, 10, temp.getWidth(), temp.getHeight(), this);
	}
}
