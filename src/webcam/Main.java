package webcam;

import java.awt.image.BufferedImage;

import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;

public class Main {
	// Convert/write matrix into BufferedImage
	public static BufferedImage matToBufferedImage(Mat matrix) {
		int cols = matrix.cols();
		int rows = matrix.rows();
		int elemSize = (int)matrix.elemSize();
		byte[] data = new byte[cols * rows * elemSize];
		int type;
		matrix.get(0, 0, data);
		switch(matrix.channels()) {
			case 1:
				type = BufferedImage.TYPE_BYTE_GRAY;
				break;
			case 3:
				type = BufferedImage.TYPE_3BYTE_BGR;
				byte b;
				for(int i=0; i<data.length; i=i+3) {
					b = data[i];
					data[i] = data[i+2];
					data[i+2] = b;
				}
				break;
			default:
				return null;
		}
		BufferedImage image2 = new BufferedImage(cols, rows, type);
		image2.getRaster().setDataElements(0, 0, cols, rows, data);
		return image2;
	}
	
	public static void main(String args[]) throws InterruptedException {
		GUI gui = new GUI("Webcam video");
		
		System.out.println("Hello OpenCV");
		System.loadLibrary("opencv_java248");
		
		Mat webcam_image = new Mat();
		BufferedImage temp;
		VideoCapture capture = new VideoCapture(0);
		if(capture.isOpened()) {
			while(true) {
				capture.read(webcam_image);
				if(!webcam_image.empty()) {
					temp = matToBufferedImage(webcam_image);
					gui.setImage(temp);
					gui.repaint();
				} else {
					System.out.println("Nothing captured");
					break;
				}
			}
		}
		capture.release();
	}
}
